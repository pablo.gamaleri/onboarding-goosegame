package com.etermax.goosegame

import com.etermax.goosegame.domain.*

class GoosegameApplication

fun main(args: Array<String>) {
	val board = createBoard()
	printRules(board)
}

fun createBoard(): Board {
	val spaces = DefaultSpaceCreator().create(rulesOrderedByPriorityDesc = defineRules(), totalPositions = 70)
	return DefaultBoard(spaces)
}

fun defineRules(): List<Rule> {
	return listOf(
			BridgeRule(),
			HotelRule(),
			WellRule(),
			MazeRule(),
			PrisonRule(),
			DeathRule(),
			FinishRule(),
			BeyondFinishRule(),
			MoveByTwoRule()
	)
}

fun printRules(board: Board) {
	board.spaces().forEach { println(it.rule.description) }
}
