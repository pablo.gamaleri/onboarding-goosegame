package com.etermax.goosegame.domain

interface SpaceCreator{
    val maxPositions: Int
    fun create(rules: List<Rule>, totalPositions: Int): List<Space>
}

class DefaultSpaceCreator: SpaceCreator {
    override val maxPositions: Int
        get() = 100

    override fun create(
            rulesOrderedByPriorityDesc: List<Rule>,
            totalPositions: Int)
    : List<Space> {
        if(totalPositions !in 1..maxPositions){
            throw IllegalArgumentException()
        }
        val spaces = ArrayList<Space>()
        for(i in 1..totalPositions){
            spaces.add(getValidSpaceRule(rulesOrderedByPriorityDesc, i))
        }
        return spaces
    }

    private fun getValidSpaceRule(rules: List<Rule>, position: Int): Space {
        for(rule in rules){
            if(rule.isPositionValid(position)){
                return DefaultSpace(rule)
            }
        }
        return DefaultSpace(DefaultRule(position))
    }

}

