package com.etermax.goosegame.domain

import com.etermax.goosegame.domain.error.PositionOutOfRange

interface Board {
    fun spaces(): List<Space>
    fun space(position: Int): Space
}

class DefaultBoard(private val spaces: List<Space>): Board {

    override fun spaces(): List<Space> {
        return spaces
    }

    override fun space(position: Int): Space {
        if (position !in 1..spaces.size) {
            throw PositionOutOfRange()
        }
        return spaces[position - 1]
    }
}