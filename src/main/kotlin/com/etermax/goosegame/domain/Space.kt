package com.etermax.goosegame.domain

interface Space {
    val rule: Rule
}

class DefaultSpace(override val rule: Rule): Space