package com.etermax.goosegame.domain.error

import java.lang.RuntimeException

class PositionOutOfRange : RuntimeException()
