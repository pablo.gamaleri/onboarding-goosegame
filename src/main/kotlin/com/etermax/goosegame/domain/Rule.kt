package com.etermax.goosegame.domain

interface Rule {
    val description: String
    fun isPositionValid(position: Int): Boolean
}

class DefaultRule(position: Int): Rule {
    override val description: String = "Stay in space $position"
    override fun isPositionValid(position: Int): Boolean = position == 6
}

class BridgeRule: Rule {
    override val description: String = "The Bridge: Go to space 12"
    override fun isPositionValid(position: Int): Boolean = position == 6
}

class HotelRule: Rule {
    override val description: String = "The Hotel: Stay for (miss) one turn"
    override fun isPositionValid(position: Int): Boolean = position == 19
}

class WellRule: Rule {
    override val description: String = "The Well: Wait until someone comes to pull you out - they then take your place"
    override fun isPositionValid(position: Int): Boolean = position == 31
}

class MazeRule: Rule {
    override val description: String = "The Maze: Go back to space 39"
    override fun isPositionValid(position: Int): Boolean = position == 42
}

class PrisonRule: Rule {
    override val description: String = "The Prison: Wait until someone comes to release you - they then take your place"
    override fun isPositionValid(position: Int): Boolean = position in 50..55
}

class MoveByTwoRule: Rule {
    override val description: String = "Move two spaces forward"
    override fun isPositionValid(position: Int): Boolean = position % 6 == 0
}

class DeathRule: Rule {
    override val description: String = "Death: Return your piece to the beginning - start the game again"
    override fun isPositionValid(position: Int): Boolean = position == 58
}

class FinishRule: Rule {
    override val description: String = "Finish: you ended the game"
    override fun isPositionValid(position: Int): Boolean = position == 63
}

class BeyondFinishRule: Rule {
    override val description: String = "Move to space 53 and stay in prison for two turns"
    override fun isPositionValid(position: Int): Boolean = position > 63
}