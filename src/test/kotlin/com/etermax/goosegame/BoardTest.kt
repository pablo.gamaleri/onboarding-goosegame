package com.etermax.goosegame

import com.etermax.goosegame.domain.*
import com.etermax.goosegame.domain.error.PositionOutOfRange
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.junit.jupiter.params.provider.ValueSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BoardTest {

	private val board = boardWithDefRules()

	@ParameterizedTest
	@ValueSource(ints = [1,2,6,55])
	fun `get default rule from board with default rules`(position: Int){
		val actualRule = board.space(position).rule
		assertEquals("Stay in space $position", actualRule.description)
	}

	@ParameterizedTest
	@ValueSource(ints = [-1, 0, 9999])
	fun `get rule for position out of range`(position: Int){
		assertThrows<PositionOutOfRange> { board.space(position) }
	}

	@ParameterizedTest
	@MethodSource("boardDataSource")
	fun `rules for the same position are applied in order`(data: BoardTestData){
		val actualRule = data.board.space(data.position).rule
		assertEquals(data.expected, actualRule.description)
	}

	private fun boardWithDefRules(): Board {
		val spaces = DefaultSpaceCreator().create(listOf(), totalPositions = 63)
		return DefaultBoard(spaces)
	}

	fun boardDataSource() = Stream.of(
			BoardTestData(board = boardWithRules(listOf(BridgeRule(), MoveByTwoRule())), position = 6, expected = BridgeRule().description),
			BoardTestData(board = boardWithRules(listOf(MoveByTwoRule(), BridgeRule())), position = 6, expected = MoveByTwoRule().description)
	)

	private fun boardWithRules(rules: List<Rule>): Board {
		val spaces = DefaultSpaceCreator().create(rules, totalPositions = 63)
		return DefaultBoard(spaces)
	}

	data class BoardTestData(
			val board: Board,
			val position: Int,
			val expected: String
	)
}
