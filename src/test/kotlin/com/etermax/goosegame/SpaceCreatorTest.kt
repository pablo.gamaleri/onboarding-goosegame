package com.etermax.goosegame

import com.etermax.goosegame.domain.DefaultSpaceCreator
import com.etermax.goosegame.domain.SpaceCreator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SpaceCreatorTest {

    private val spaceCreator: SpaceCreator = DefaultSpaceCreator()

    @ParameterizedTest
    @ValueSource(ints = [1, 2, 6, 63, 99])
    fun `quantity of spaces is created correctly`(totalPositions: Int){
        val spaces = spaceCreator.create(listOf(), totalPositions)
        assertEquals(totalPositions, spaces.size)
    }

    @ParameterizedTest
    @ValueSource(ints = [-100, -1, 0, 101, 9999])
    fun `invalid parameter for total of positions to be created`(totalPositions: Int){
        assertThrows<IllegalArgumentException> { spaceCreator.create(listOf(), totalPositions) }
    }

}