package com.etermax.goosegame

import com.etermax.goosegame.domain.BridgeRule
import com.etermax.goosegame.domain.MoveByTwoRule
import com.etermax.goosegame.domain.Rule
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RuleTest{

    @ParameterizedTest
    @MethodSource("ruleDataSource")
    fun `position validation`(data: RuleTestData){
        val rule = data.rule
        Assertions.assertEquals(data.expected, rule.isPositionValid(data.position))
    }

    fun ruleDataSource() = Stream.of(
            RuleTestData(MoveByTwoRule(), position = 12, expected = true),
            RuleTestData(MoveByTwoRule(), position = 13, expected = false),
            RuleTestData(BridgeRule(), position = 6, expected = true),
            RuleTestData(BridgeRule(), position = 1, expected = false)
    )

    data class RuleTestData(
            val rule: Rule,
            val position: Int,
            val expected: Boolean
    )
}